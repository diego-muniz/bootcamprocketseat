const express = require('express');

const app = express();

app.use(express.json());

const projects = [
    { id: "1", title: "teste de projeto", tasks:['testando task projeto']},
    { id: "2", title: "projeto de teste", tasks:['task projeto testando']}
];

var countRequest = 0;
app.use((req, res, next) => {
    console.log(`${++countRequest} - request`);
    next();
})

function existsProject(req, res, next) {
    const { id } = req.params;

    if (!projects.find(x => x.id == id)) {
        return res.status(400).json({ error: 'Not find project'})
    } 

    next();

}

app.get('/projects', (req, res) => {
    return res.json(projects);
});

app.post('/projects', (req, res) => {
    const  { id, title, tasks }  = req.body;

    projects.push({ id: id, title: title, tasks: []});

    return res.json(projects);

});

app.post('/projects/:id/tasks', (req, res) => {
    const { id } = req.params;
    const { task } = req.body;

    projects.filter(n => n.id == id)
            .map(n => {
                n.tasks.push(task)
            });

    return res.json(projects);
})

app.put('/projects/:id', (req, res) => {
    const { title, tasks } = req.body;
    const { id } = req.params;

    projects.filter(n => n.id == id)
            .map(n => {
                n.title = title,
                n.tasks = tasks
            });

    return res.json(projects);
});

app.delete('/projects/:id', existsProject, (req, res) => {
    const { id } = req.params;

    const index = projects.findIndex(n => n.id == id);

    projects.splice(index, 1);

    return res.json(projects);
})


app.listen(2000);