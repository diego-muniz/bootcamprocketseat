module.exports = {
  presets: [
    "@babel/preset-env", // alterar tudo que o navegador nao entende export import etc.
    "@babel/preset-react",  // alterar tudo que o navegador nao entende do react
  ],
  plugins: [
    '@babel/plugin-proposal-class-properties'
  ],
}