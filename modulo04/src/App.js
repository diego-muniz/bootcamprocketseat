import React from 'react';
import './App.css';
import dev from './assets/dev.jpg'

import TechList from './components/TechList';

function App() {
  return <TechList />
}

export default App;